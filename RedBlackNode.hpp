﻿#ifndef REDBLACKNODE_HPP
#define REDBLACKNODE_HPP

#include "Node.hpp"

template <class T> class RedBlackTree;

template <class T> class RedBlackNode : public Node<T> {
public:
  enum NodeColor : char { Black, DoubleBlack, Red };

  static RedBlackNode<T> *parent(RedBlackNode<T> *);
  static NodeColor nodeColor(RedBlackNode<T> *);

  RedBlackNode(RedBlackTree<T> *);
  RedBlackNode(RedBlackTree<T> *, const T &);

  RedBlackNode<T> *parent() const;
  void setParent(RedBlackNode<T> *);

  RedBlackNode<T> *left() const override;
  RedBlackNode<T> *right() const override;

  QColor color() const override;

  NodeColor nodeColor() const;
  void setNodeColor(NodeColor);
  void swapNodeColors(RedBlackNode<T> *);

private:
  RedBlackNode<T> *_parent;
  NodeColor _nodeColor;
};

#include "RedBlackTree.hpp"

template <class T>
RedBlackNode<T> *RedBlackNode<T>::parent(RedBlackNode<T> *node) {
  return node == nullptr ? nullptr : node->parent();
}

template <class T>
typename RedBlackNode<T>::NodeColor
RedBlackNode<T>::nodeColor(RedBlackNode<T> *node) {
  return node == nullptr ? Black : node->nodeColor();
}

template <class T>
RedBlackNode<T>::RedBlackNode(RedBlackTree<T> *tree)
    : Node<T>(tree), _parent(nullptr), _nodeColor(Red) {}

template <class T>
RedBlackNode<T>::RedBlackNode(RedBlackTree<T> *tree, const T &value)
    : Node<T>(tree, value), _parent(nullptr), _nodeColor(Red) {}

template <class T> RedBlackNode<T> *RedBlackNode<T>::parent() const {
  return _parent;
}

template <class T> void RedBlackNode<T>::setParent(RedBlackNode<T> *newParent) {
  _parent = newParent;
}

template <class T> RedBlackNode<T> *RedBlackNode<T>::left() const {
  return static_cast<RedBlackNode<T> *>(Node<T>::left());
}

template <class T> RedBlackNode<T> *RedBlackNode<T>::right() const {
  return static_cast<RedBlackNode<T> *>(Node<T>::right());
}

template <class T> QColor RedBlackNode<T>::color() const {
  return nodeColor() == Red ? QColor(244, 67, 54) : QColor(16, 16, 16);
}

template <class T>
typename RedBlackNode<T>::NodeColor RedBlackNode<T>::nodeColor() const {
  return _nodeColor;
}

template <class T>
void RedBlackNode<T>::setNodeColor(
    typename RedBlackNode<T>::NodeColor newColor) {
  _nodeColor = newColor;
  this->update();
}

template <class T> void RedBlackNode<T>::swapNodeColors(RedBlackNode<T> *node) {
  NodeColor temp = _nodeColor;
  _nodeColor = node->_nodeColor;
  node->_nodeColor = temp;
  this->update();
  node->update();
}

#endif // REDBLACKNODE_HPP

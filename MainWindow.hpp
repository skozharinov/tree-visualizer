﻿#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>

class QPushButton;
class QRadioButton;
class QLabel;
class QStackedWidget;

class ValueEdit;
template <class T> class SearchTree;
template <class T> class RandomizedTree;
template <class T> class AVLTree;
template <class T> class RedBlackTree;
template <class T> class SplayTree;
template <class T> class SkewedTree;

class MainWindow : public QMainWindow {
  Q_OBJECT
public:
  MainWindow(QWidget * = nullptr);
  quint32 treeSize() const;

public slots:
  void push();
  void pop();
  void previous();
  void next();
  void get();
  void minimum();
  void maximum();
  void clear();
  void switchTree();
  void updateSize();
  void zoomIn();
  void zoomOut();

signals:
  void sizeChanged();

private:
  ValueEdit *_valueEdit;
  QPushButton *_randomButton;
  QPushButton *_pushButton;
  QPushButton *_popButton;
  QPushButton *_previousButton;
  QPushButton *_nextButton;
  QPushButton *_getButton;
  QPushButton *_minimumButton;
  QPushButton *_maximumButton;
  QPushButton *_clearButton;
  QPushButton *_zoomInButton;
  QPushButton *_zoomOutButton;
  QRadioButton *_searchTreeRadio;
  QRadioButton *_randomizedTreeRadio;
  QRadioButton *_avlTreeRadio;
  QRadioButton *_redBlackTreeRadio;
  QRadioButton *_splayTreeRadio;
  QRadioButton *_skewedTreeRadio;
  QStackedWidget *_treeWidget;
  QLabel *_sizeLabel;

  SearchTree<qint32> *_searchTree;
  RandomizedTree<qint32> *_randomizedTree;
  AVLTree<qint32> *_avlTree;
  RedBlackTree<qint32> *_redBlackTree;
  SplayTree<qint32> *_splayTree;
  SkewedTree<qint32> *_skewedTree;
};

#endif // MAINWINDOW_HPP

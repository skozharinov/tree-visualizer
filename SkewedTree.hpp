﻿#ifndef SKEWEDTREE_HPP
#define SKEWEDTREE_HPP

#include "SearchTree.hpp"

template <class T> class SkewedNode;

template <class T> class SkewedTree : public SearchTree<T> {
public:
  void push(const T &);
  void pop(const T &);

protected:
  SkewedNode<T> *doPush(const T &);
  SkewedNode<T> *doPop(const T &);

private:
  SkewedNode<T> *cast(Node<T> *);
};

#include "SkewedNode.hpp"

template <class T> void SkewedTree<T>::push(const T &value) {
  Tree<T>::highlight();
  SkewedNode<T> *node = doPush(value);
  Tree<T>::adjust();
  Tree<T>::highlight(node);
}

template <class T> void SkewedTree<T>::pop(const T &value) {
  Tree<T>::highlight();
  doPop(value);
  Tree<T>::adjust();
}

template <class T> SkewedNode<T> *SkewedTree<T>::doPush(const T &value) {
  if (Tree<T>::root() == nullptr) {
    Tree<T>::setRoot(new SkewedNode<T>(this, value));
    return cast(Tree<T>::root());
  } else {
    SkewedNode<T> *parent = nullptr;
    SkewedNode<T> *node = cast(Tree<T>::root());

    while (node != nullptr && value < node->value()) {
      parent = node;
      node = node->left();
    }

    if (node != nullptr && value == node->value())
      return nullptr;

    SkewedNode<T> *temp = new SkewedNode<T>(this, value);

    if (parent != nullptr) {
      parent->setLeft(temp);
    } else {
      Tree<T>::setRoot(temp);
    }

    if (node != nullptr) {
      temp->setLeft(node);
    }

    return temp;
  }
}

template <class T> SkewedNode<T> *SkewedTree<T>::doPop(const T &value) {
  if (Tree<T>::root() == nullptr) {
    return nullptr;
  } else {
    SkewedNode<T> *parent = nullptr;
    SkewedNode<T> *node = cast(Tree<T>::root());

    while (node != nullptr && value < node->value()) {
      parent = node;
      node = node->left();
    }

    if (node == nullptr || value != node->value())
      return nullptr;

    SkewedNode<T> *temp = node->left();

    if (parent != nullptr) {
      parent->setLeft(temp);
    } else {
      Tree<T>::setRoot(temp);
    }

    delete node;
    return nullptr;
  }
}

template <class T> SkewedNode<T> *SkewedTree<T>::cast(Node<T> *node) {
  return static_cast<SkewedNode<T> *>(node);
}

#endif // SKEWEDTREE_HPP

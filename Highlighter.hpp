﻿#ifndef HIGHLIGHTER_HPP
#define HIGHLIGHTER_HPP

#include <QGraphicsItem>

template <class T> class Node;

template <class T> class Highlighter : public QGraphicsItem {
public:
  Highlighter();

  Node<T> *node() const;
  void setNode(Node<T> *);

protected:
  QRectF boundingRect() const override;
  QPainterPath shape() const override;
  void paint(QPainter *, const QStyleOptionGraphicsItem *, QWidget *) override;

private:
  static constexpr const qreal Outline = 4;
  static constexpr const qreal Radius = Node<T>::Radius;
  Node<T> *_node;
};

#include <QPainter>

template <class T> Highlighter<T>::Highlighter() {
  setZValue(2);

  _node = nullptr;
}

template <class T> QRectF Highlighter<T>::boundingRect() const {
  if (node() == nullptr)
    return QRectF();
  const QPointF delta(Radius + Outline, Radius + Outline);
  return QRectF(node()->pos() - delta, node()->pos() + delta);
}

template <class T> QPainterPath Highlighter<T>::shape() const {
  const qreal radius = Radius + Outline / 2;

  QPainterPath path;
  path.addEllipse(node()->pos(), radius, radius);
  return path;
}

template <class T>
void Highlighter<T>::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                           QWidget *) {
  if (node() != nullptr) {
    const qreal radius = Radius + Outline / 2;

    painter->setPen(QPen(Qt::white, Outline));
    painter->setBrush(Qt::NoBrush);
    painter->drawEllipse(node()->pos(), radius, radius);
  }
}

template <class T> Node<T> *Highlighter<T>::node() const { return _node; }

template <class T> void Highlighter<T>::setNode(Node<T> *newNode) {
  _node = newNode;
  prepareGeometryChange();
}

#endif // HIGHLIGHTER_HPP

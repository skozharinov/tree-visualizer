﻿#ifndef SPLAYTREE_HPP
#define SPLAYTREE_HPP

#include "SearchTree.hpp"

template <class T> class SplayNode;

template <class T> class SplayTree : public SearchTree<T> {
public:
  void push(const T &);
  void pop(const T &);
  void get(const T &);
  void minimum();
  void maximum();
  void previous(const T &);
  void next(const T &);

protected:
  SplayNode<T> *splay(SplayNode<T> *, const T &);
  SplayNode<T> *splayMinimum(SplayNode<T> *);
  SplayNode<T> *splayMaximum(SplayNode<T> *);

private:
  SplayNode<T> *cast(Node<T> *);
};

#include "SplayNode.hpp"

template <class T> void SplayTree<T>::push(const T &value) {
  Tree<T>::highlight();
  Tree<T>::setRoot(splay(cast(Tree<T>::root()), value));
  if (Tree<T>::root() == nullptr) {
    Tree<T>::setRoot(new SplayNode<T>(this, value));
  } else if (value == Tree<T>::root()->value()) {
    return;
  } else if (value < Tree<T>::root()->value()) {
    SplayNode<T> *left = cast(Tree<T>::root())->left();
    SplayNode<T> *right = cast(Tree<T>::root());
    Tree<T>::root()->setLeft(nullptr);
    Tree<T>::setRoot(new SplayNode<T>(this, value));
    Tree<T>::root()->setLeft(left);
    Tree<T>::root()->setRight(right);
  } else {
    SplayNode<T> *left = cast(Tree<T>::root());
    SplayNode<T> *right = cast(Tree<T>::root())->right();
    Tree<T>::root()->setRight(nullptr);
    Tree<T>::setRoot(new SplayNode<T>(this, value));
    Tree<T>::root()->setLeft(left);
    Tree<T>::root()->setRight(right);
  }

  Tree<T>::adjust();
  Tree<T>::highlight(Tree<T>::root());
}

template <class T> void SplayTree<T>::pop(const T &value) {
  Tree<T>::highlight();
  Tree<T>::setRoot(splay(cast(Tree<T>::root()), value));

  if (Tree<T>::root() == nullptr || value != Tree<T>::root()->value())
    return;

  SplayNode<T> *temp = cast(Tree<T>::root());

  if (Tree<T>::root()->left() == nullptr) {
    Tree<T>::setRoot(temp->right());
  } else {
    Tree<T>::setRoot(splay(cast(Tree<T>::root())->left(), value));
    Tree<T>::root()->setRight(temp->right());
  }

  delete temp;
  Tree<T>::adjust();
}

template <class T> void SplayTree<T>::get(const T &value) {
  Tree<T>::highlight();

  if (Tree<T>::root() == nullptr)
    return;

  Tree<T>::setRoot(splay(cast(Tree<T>::root()), value));
  Tree<T>::adjust();

  if (value == Tree<T>::root()->value())
    Tree<T>::highlight(Tree<T>::root());
}

template <class T> void SplayTree<T>::minimum() {
  Tree<T>::highlight();

  if (Tree<T>::root() == nullptr)
    return;

  Tree<T>::setRoot(splayMinimum(cast(Tree<T>::root())));
  Tree<T>::adjust();
  Tree<T>::highlight(Tree<T>::root());
}

template <class T> void SplayTree<T>::maximum() {
  Tree<T>::highlight();

  if (Tree<T>::root() == nullptr)
    return;

  Tree<T>::setRoot(splayMaximum(cast(Tree<T>::root())));
  Tree<T>::adjust();
  Tree<T>::highlight(Tree<T>::root());
}

template <class T> void SplayTree<T>::previous(const T &value) {
  Tree<T>::highlight();

  if (Tree<T>::root() == nullptr)
    return;

  Tree<T>::setRoot(splay(cast(Tree<T>::root()), value));
  Tree<T>::adjust();

  if (Tree<T>::root()->left() == nullptr)
    return;

  if (value == Tree<T>::root()->value()) {
    Tree<T>::root()->setLeft(splayMaximum(cast(Tree<T>::root())->left()));
    Tree<T>::setRoot(
        splay(cast(Tree<T>::root()), Tree<T>::root()->left()->value()));
    Tree<T>::adjust();
  }

  Tree<T>::highlight(Tree<T>::root());
}

template <class T> void SplayTree<T>::next(const T &value) {
  Tree<T>::highlight();

  if (Tree<T>::root() == nullptr)
    return;

  Tree<T>::setRoot(splay(cast(Tree<T>::root()), value));
  Tree<T>::adjust();

  if (Tree<T>::root()->right() == nullptr)
    return;

  if (value == Tree<T>::root()->value()) {
    Tree<T>::root()->setRight(splayMinimum(cast(Tree<T>::root())->right()));
    Tree<T>::setRoot(
        splay(cast(Tree<T>::root()), Tree<T>::root()->right()->value()));
    Tree<T>::adjust();
  }

  Tree<T>::highlight(Tree<T>::root());
}

template <class T>
SplayNode<T> *SplayTree<T>::splay(SplayNode<T> *node, const T &value) {
  if (node == nullptr || value == node->value()) {
    return node;
  } else if (value < node->value()) {
    if (node->left() == nullptr) {
      return node;
    } else if (value < node->left()->value()) {
      node->left()->setLeft(splay(node->left()->left(), value));
      node = cast(SearchTree<T>::rotateRight(node));
    } else if (value > node->left()->value()) {
      node->left()->setRight(splay(node->left()->right(), value));

      if (node->left()->right() != nullptr) {
        node->setLeft(cast(SearchTree<T>::rotateLeft(node->left())));
      }
    }

    if (node->left() == nullptr)
      return node;

    return cast(SearchTree<T>::rotateRight(node));
  } else {
    if (node->right() == nullptr) {
      return node;
    } else if (value < node->right()->value()) {
      node->right()->setLeft(splay(node->right()->left(), value));

      if (node->right()->left() != nullptr) {
        node->setRight(cast(SearchTree<T>::rotateRight(node->right())));
      }
    } else if (value > node->right()->value()) {
      node->right()->setRight(splay(node->right()->right(), value));
      node = cast(SearchTree<T>::rotateLeft(node));
    }

    if (node->right() == nullptr)
      return node;

    return cast(SearchTree<T>::rotateLeft(node));
  }
}

template <class T>
SplayNode<T> *SplayTree<T>::splayMinimum(SplayNode<T> *node) {
  if (node == nullptr || node->left() == nullptr)
    return node;

  node->left()->setLeft(splayMinimum(node->left()->left()));
  node = cast(SearchTree<T>::rotateRight(node));

  if (node->left() == nullptr)
    return node;

  return cast(SearchTree<T>::rotateRight(node));
}

template <class T>
SplayNode<T> *SplayTree<T>::splayMaximum(SplayNode<T> *node) {
  if (node == nullptr || node->right() == nullptr)
    return node;

  node->right()->setRight(splayMaximum(node->right()->right()));
  node = cast(SearchTree<T>::rotateLeft(node));

  if (node->right() == nullptr)
    return node;

  return cast(SearchTree<T>::rotateLeft(node));
}

template <class T> SplayNode<T> *SplayTree<T>::cast(Node<T> *node) {
  return static_cast<SplayNode<T> *>(node);
}

#endif // SPLAYTREE_HPP

﻿#ifndef RANDOMIZEDNODE_HPP
#define RANDOMIZEDNODE_HPP

#include "Node.hpp"

template <class T> class RandomizedTree;

template <class T> class RandomizedNode : public Node<T> {
public:
  RandomizedNode(RandomizedTree<T> *);
  RandomizedNode(RandomizedTree<T> *, const T &);

  RandomizedNode<T> *left() const override;
  RandomizedNode<T> *right() const override;

  quint16 priority() const;

  QColor color() const override;

private:
  quint16 _priority;
};

#include "RandomizedTree.hpp"
#include <QRandomGenerator>

template <class T>
RandomizedNode<T>::RandomizedNode(RandomizedTree<T> *tree)
    : Node<T>(tree),
      _priority(static_cast<quint16>(QRandomGenerator::global()->generate())) {}

template <class T>
RandomizedNode<T>::RandomizedNode(RandomizedTree<T> *tree, const T &value)
    : Node<T>(tree, value),
      _priority(static_cast<quint16>(QRandomGenerator::global()->generate())) {}

template <class T> RandomizedNode<T> *RandomizedNode<T>::left() const {
  return static_cast<RandomizedNode<T> *>(Node<T>::left());
}

template <class T> RandomizedNode<T> *RandomizedNode<T>::right() const {
  return static_cast<RandomizedNode<T> *>(Node<T>::right());
}

template <class T> quint16 RandomizedNode<T>::priority() const {
  return _priority;
}

template <class T> QColor RandomizedNode<T>::color() const {
  return QColor(255, 152, 0);
}

#endif // RANDOMIZEDNODE_HPP

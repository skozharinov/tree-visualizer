﻿#ifndef SEARCHTREE_HPP
#define SEARCHTREE_HPP

#include "Tree.hpp"

template <class T> class Node;

template <class T> class SearchTree : public Tree<T> {
public:
  virtual void push(const T &);
  virtual void pop(const T &);
  virtual void get(const T &);
  virtual void minimum();
  virtual void maximum();
  virtual void previous(const T &);
  virtual void next(const T &);

protected:
  Node<T> *minimum(Node<T> *) const;
  Node<T> *maximum(Node<T> *) const;
  Node<T> *get(Node<T> *, const T &) const;
  Node<T> *previous(Node<T> *, const T &) const;
  Node<T> *next(Node<T> *, const T &) const;
  Node<T> *newNode(const T &) const;
  Node<T> *push(Node<T> *, const T &);
  Node<T> *pop(Node<T> *, const T &);
  Node<T> *rotateLeft(Node<T> *);
  Node<T> *rotateRight(Node<T> *);
};

#include "Node.hpp"

template <class T> void SearchTree<T>::push(const T &value) {
  Tree<T>::highlight();
  Tree<T>::setRoot(push(Tree<T>::root(), value));
  Tree<T>::adjust();
}

template <class T> void SearchTree<T>::pop(const T &value) {
  Tree<T>::highlight();
  Tree<T>::setRoot(pop(Tree<T>::root(), value));
  Tree<T>::adjust();
}

template <class T> void SearchTree<T>::get(const T &value) {
  Tree<T>::highlight();
  Node<T> *node = get(Tree<T>::root(), value);
  if (node != nullptr)
    Tree<T>::highlight(node);
}

template <class T> void SearchTree<T>::minimum() {
  Tree<T>::highlight();
  Node<T> *node = minimum(Tree<T>::root());
  if (node != nullptr)
    Tree<T>::highlight(node);
}

template <class T> void SearchTree<T>::maximum() {
  Tree<T>::highlight();
  Node<T> *node = maximum(Tree<T>::root());
  if (node != nullptr)
    Tree<T>::highlight(node);
}

template <class T> void SearchTree<T>::previous(const T &value) {
  Tree<T>::highlight();
  Node<T> *node = previous(Tree<T>::root(), value);
  if (node != nullptr)
    Tree<T>::highlight(node);
}

template <class T> void SearchTree<T>::next(const T &value) {
  Tree<T>::highlight();
  Node<T> *node = next(Tree<T>::root(), value);
  if (node != nullptr)
    Tree<T>::highlight(node);
}

template <class T> Node<T> *SearchTree<T>::newNode(const T &value) const {
  Node<T> *node = Tree<T>::newNode(value);
  node->setColor(Qt::darkGray);
  return node;
}

template <class T>
Node<T> *SearchTree<T>::get(Node<T> *node, const T &value) const {
  if (node == nullptr)
    return nullptr;
  if (value < node->value())
    return get(node->left(), value);
  if (value > node->value())
    return get(node->right(), value);
  return node;
}

template <class T> Node<T> *SearchTree<T>::minimum(Node<T> *node) const {
  if (node == nullptr || node->left() == nullptr)
    return node;
  return minimum(node->left());
}

template <class T> Node<T> *SearchTree<T>::maximum(Node<T> *node) const {
  if (node == nullptr || node->right() == nullptr)
    return node;
  return maximum(node->right());
}

template <class T>
Node<T> *SearchTree<T>::previous(Node<T> *node, const T &value) const {
  if (node == nullptr)
    return nullptr;
  if (value < node->value())
    return previous(node->left(), value);
  if (value == node->value())
    return maximum(node->left());
  Node<T> *temp = previous(node->right(), value);
  if (temp == nullptr)
    return node;
  return temp;
}

template <class T>
Node<T> *SearchTree<T>::next(Node<T> *node, const T &value) const {
  if (node == nullptr)
    return nullptr;
  if (value > node->value())
    return next(node->right(), value);
  if (value == node->value())
    return minimum(node->right());
  Node<T> *temp = next(node->left(), value);
  if (temp == nullptr)
    return node;
  return temp;
}

template <class T> Node<T> *SearchTree<T>::push(Node<T> *node, const T &value) {
  if (node == nullptr) {
    node = new Node<T>(this, value);
    Tree<T>::highlight(node);
  } else if (value < node->value()) {
    node->setLeft(push(node->left(), value));
  } else if (value > node->value()) {
    node->setRight(push(node->right(), value));
  } else {
    return node;
  }

  return node;
}

template <class T> Node<T> *SearchTree<T>::pop(Node<T> *node, const T &value) {
  if (node == nullptr) {
    return node;
  } else if (value < node->value()) {
    node->setLeft(pop(node->left(), value));
  } else if (value > node->value()) {
    node->setRight(pop(node->right(), value));
  } else if (node->left() == nullptr) {
    Node<T> *temp = node->right();
    delete node;
    node = temp;
  } else if (node->right() == nullptr) {
    Node<T> *temp = node->left();
    delete node;
    node = temp;
  } else {
    Node<T> *temp = minimum(node->right());
    node->swapValues(temp);
    node->setRight(pop(node->right(), temp->value()));
  }

  return node;
}

template <class T> Node<T> *SearchTree<T>::rotateLeft(Node<T> *node) {
  Node<T> *temp = node->right();
  node->setRight(temp->left());
  temp->setLeft(node);
  return temp;
}

template <class T> Node<T> *SearchTree<T>::rotateRight(Node<T> *node) {
  Node<T> *temp = node->left();
  node->setLeft(temp->right());
  temp->setRight(node);
  return temp;
}

#endif // SEARCHTREE_HPP

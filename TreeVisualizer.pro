QT += core gui widgets

TARGET = TreeVisualizer
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000

CONFIG += c++11

SOURCES += main.cpp
SOURCES += DraggableView.cpp
SOURCES += MainWindow.cpp
SOURCES += ValueEdit.cpp
SOURCES += ZoomableView.cpp

HEADERS += AVLNode.hpp
HEADERS += AVLTree.hpp
HEADERS += DraggableView.hpp
HEADERS += Edges.hpp
HEADERS += Highlighter.hpp
HEADERS += MainWindow.hpp
HEADERS += Node.hpp
HEADERS += RandomizedNode.hpp
HEADERS += RandomizedTree.hpp
HEADERS += RedBlackNode.hpp
HEADERS += RedBlackTree.hpp
HEADERS += SearchTree.hpp
HEADERS += SkewedNode.hpp
HEADERS += SkewedTree.hpp
HEADERS += SplayNode.hpp
HEADERS += SplayTree.hpp
HEADERS += Tree.hpp
HEADERS += ValueEdit.hpp
HEADERS += ZoomableView.hpp

RESOURCES += resources.qrc

TRANSLATIONS += translation_ru.ts


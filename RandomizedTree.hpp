﻿#ifndef RANDOMIZEDTREE_HPP
#define RANDOMIZEDTREE_HPP

#include "SearchTree.hpp"

template <class T> class RandomizedNode;

template <class T> class RandomizedTree : public SearchTree<T> {
public:
  void push(const T &) override;
  void pop(const T &) override;

protected:
  RandomizedNode<T> *push(RandomizedNode<T> *, const T &);
  RandomizedNode<T> *pop(RandomizedNode<T> *, const T &);

private:
  RandomizedNode<T> *cast(Node<T> *);
};

#include "RandomizedNode.hpp"

template <class T> void RandomizedTree<T>::push(const T &value) {
  Tree<T>::highlight();
  Tree<T>::setRoot(push(cast(Tree<T>::root()), value));
  Tree<T>::adjust();
}

template <class T> void RandomizedTree<T>::pop(const T &value) {
  Tree<T>::highlight();
  Tree<T>::setRoot(pop(cast(Tree<T>::root()), value));
  Tree<T>::adjust();
}

template <class T>
RandomizedNode<T> *RandomizedTree<T>::push(RandomizedNode<T> *node,
                                           const T &value) {
  if (node == nullptr) {
    node = new RandomizedNode<T>(this, value);
    Tree<T>::highlight(node);
    return node;
  } else if (value < node->value()) {
    node->setLeft(push(node->left(), value));

    if (node->left()->priority() > node->priority()) {
      node = cast(SearchTree<T>::rotateRight(node));
    }
  } else if (value > node->value()) {
    node->setRight(push(node->right(), value));

    if (node->right()->priority() > node->priority()) {
      node = cast(SearchTree<T>::rotateLeft(node));
    }
  }

  return node;
}

template <class T>
RandomizedNode<T> *RandomizedTree<T>::pop(RandomizedNode<T> *node,
                                          const T &value) {
  if (node == nullptr) {
    return node;
  } else if (value < node->value()) {
    node->setLeft(pop(node->left(), value));
  } else if (value > node->value()) {
    node->setRight(pop(node->right(), value));
  } else {
    if (node->left() == nullptr) {
      RandomizedNode<T> *temp = node->right();
      delete node;
      node = temp;
    } else if (node->right() == nullptr) {
      RandomizedNode<T> *temp = node->left();
      delete node;
      node = temp;
    } else {
      if (node->left()->priority() < node->priority()) {
        node = cast(SearchTree<T>::rotateLeft(node));
        node->setLeft(pop(node->left(), value));
      } else {
        node = cast(SearchTree<T>::rotateRight(node));
        node->setRight(pop(node->right(), value));
      }
    }
  }

  return node;
}

template <class T> RandomizedNode<T> *RandomizedTree<T>::cast(Node<T> *node) {
  return static_cast<RandomizedNode<T> *>(node);
}

#endif // RANDOMIZEDTREE_HPP

﻿#ifndef EDGES_HPP
#define EDGES_HPP

#include <QGraphicsItem>

template <class T> class Node;

template <class T> class Edges : public QGraphicsItem {
private:
public:
  Edges(Node<T> *);

  Node<T> *node() const;
  Node<T> *left() const;
  Node<T> *right() const;

protected:
  QRectF boundingRect() const override;
  void paint(QPainter *, const QStyleOptionGraphicsItem *, QWidget *) override;

private:
  Node<T> *_node;
};

#include <QPainter>

template <class T> Edges<T>::Edges(Node<T> *node) : _node(node) {
  setZValue(-2);
  setFlags(QGraphicsItem::ItemStacksBehindParent);
}

template <class T> Node<T> *Edges<T>::node() const { return _node; }

template <class T> Node<T> *Edges<T>::left() const { return node()->left(); }

template <class T> Node<T> *Edges<T>::right() const { return node()->right(); }

template <class T> QRectF Edges<T>::boundingRect() const {
  if (left() == nullptr && right() == nullptr) {
    return QRectF();
  } else if (left() != nullptr) {
    return QRectF(left()->x(), node()->y(), node()->x() - left()->x(),
                  left()->y() - node()->y());
  } else if (right() != nullptr) {
    return QRectF(node()->x(), node()->y(), right()->x() - node()->x(),
                  right()->y() - node()->y());
  } else {
    return QRectF(left()->x(), node()->y(), right()->x() - left()->x(),
                  left()->y() - node()->y());
  }
}

template <class T>
void Edges<T>::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                     QWidget *) {
  painter->setPen(QPen(QColor(238, 238, 238), 3));

  if (left() != nullptr) {
    painter->drawLine(node()->pos(), left()->pos());
  }

  if (right() != nullptr) {
    painter->drawLine(node()->pos(), right()->pos());
  }
}

#endif // EDGES_HPP

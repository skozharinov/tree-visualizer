﻿#ifndef AVLTREE_HPP
#define AVLTREE_HPP

#include "SearchTree.hpp"

template <class T> class AVLNode;

template <class T> class AVLTree : public SearchTree<T> {
public:
  void push(const T &);
  void pop(const T &);

protected:
  AVLNode<T> *push(AVLNode<T> *, const T &);
  AVLNode<T> *pop(AVLNode<T> *, const T &);

  AVLNode<T> *rotateLeft(AVLNode<T> *);
  AVLNode<T> *rotateRight(AVLNode<T> *);

  qint8 balance(AVLNode<T> *) const;

private:
  AVLNode<T> *cast(Node<T> *);
};

#include "AVLNode.hpp"

template <class T> void AVLTree<T>::push(const T &value) {
  Tree<T>::highlight();
  Tree<T>::setRoot(push(cast(Tree<T>::root()), value));
  Tree<T>::adjust();
}

template <class T> void AVLTree<T>::pop(const T &value) {
  Tree<T>::highlight();
  Tree<T>::setRoot(pop(cast(Tree<T>::root()), value));
  Tree<T>::adjust();
}

template <class T>
AVLNode<T> *AVLTree<T>::push(AVLNode<T> *node, const T &value) {
  if (node == nullptr) {
    node = new AVLNode<T>(this, value);
    Tree<T>::highlight(node);
  } else if (value < node->value()) {
    node->setLeft(push(node->left(), value));
  } else if (value > node->value()) {
    node->setRight(push(node->right(), value));
  } else {
    return node;
  }

  node->setHeight();

  if (balance(node) > 1) {
    if (value < node->left()->value()) {
      return rotateRight(node);
    } else if (value > node->left()->value()) {
      node->setLeft(rotateLeft(node->left()));
      return rotateRight(node);
    }
  } else if (balance(node) < -1) {
    if (value < node->right()->value()) {
      node->setRight(rotateRight(node->right()));
      return rotateLeft(node);
    } else if (value > node->right()->value()) {
      return rotateLeft(node);
    }
  }

  return node;
}

template <class T>
AVLNode<T> *AVLTree<T>::pop(AVLNode<T> *node, const T &value) {
  if (node == nullptr) {
    return node;
  } else if (value < node->value()) {
    node->setLeft(pop(node->left(), value));
  } else if (value > node->value()) {
    node->setRight(pop(node->right(), value));
  } else if (node->left() == nullptr) {
    AVLNode<T> *temp = node->right();
    delete node;
    node = temp;
  } else if (node->right() == nullptr) {
    AVLNode<T> *temp = node->left();
    delete node;
    node = temp;
  } else {
    AVLNode<T> *temp = cast(SearchTree<T>::minimum(node->right()));
    node->swapValues(temp);
    node->setRight(pop(node->right(), temp->value()));
  }

  if (node == nullptr)
    return node;
  else
    node->setHeight();

  if (balance(node) > 1) {
    if (balance(node->left()) >= 0) {
      return rotateRight(node);
    } else if (balance(node->left()) < 0) {
      node->setLeft(rotateLeft(node->left()));
      return rotateRight(node);
    }
  } else if (balance(node) < -1) {
    if (balance(node->right()) <= 0) {
      return rotateLeft(node);
    } else if (balance(node->right()) > 0) {
      node->setRight(rotateRight(node->right()));
      return rotateLeft(node);
    }
  }

  return node;
}

template <class T> qint8 AVLTree<T>::balance(AVLNode<T> *node) const {
  if (node == nullptr)
    return 0;
  qint8 left = static_cast<qint8>(AVLNode<T>::height(node->left()));
  qint8 right = static_cast<qint8>(AVLNode<T>::height(node->right()));
  return left - right;
}

template <class T> AVLNode<T> *AVLTree<T>::cast(Node<T> *node) {
  return static_cast<AVLNode<T> *>(node);
}

template <class T> AVLNode<T> *AVLTree<T>::rotateLeft(AVLNode<T> *node) {
  AVLNode<T> *temp = node->right();
  node->setRight(temp->left());
  temp->setLeft(node);
  node->setHeight();
  temp->setHeight();
  return temp;
}

template <class T> AVLNode<T> *AVLTree<T>::rotateRight(AVLNode<T> *node) {
  AVLNode<T> *temp = node->left();
  node->setLeft(temp->right());
  temp->setRight(node);
  node->setHeight();
  temp->setHeight();
  return temp;
}

#endif // AVLTREE_HPP

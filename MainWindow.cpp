﻿#include "MainWindow.hpp"
#include "AVLTree.hpp"
#include "RandomizedTree.hpp"
#include "RedBlackTree.hpp"
#include "SearchTree.hpp"
#include "SkewedTree.hpp"
#include "SplayTree.hpp"
#include "ValueEdit.hpp"
#include <QGroupBox>
#include <QLabel>
#include <QPushButton>
#include <QRadioButton>
#include <QSizePolicy>
#include <QStackedWidget>
#include <QStatusBar>
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
  setWindowIcon(QIcon("://icon.svg"));

  QWidget *mainWidget = new QWidget();

  _treeWidget = new QStackedWidget();

  _searchTree = new SearchTree<qint32>();
  _randomizedTree = new RandomizedTree<qint32>();
  _avlTree = new AVLTree<qint32>();
  _redBlackTree = new RedBlackTree<qint32>();
  _splayTree = new SplayTree<qint32>();
  _skewedTree = new SkewedTree<qint32>();

  _treeWidget->addWidget(_searchTree);
  _treeWidget->addWidget(_randomizedTree);
  _treeWidget->addWidget(_avlTree);
  _treeWidget->addWidget(_redBlackTree);
  _treeWidget->addWidget(_splayTree);
  _treeWidget->addWidget(_skewedTree);

  QGroupBox *valueActionsBox = new QGroupBox(tr("Number Actions"));

  _valueEdit = new ValueEdit();
  _valueEdit->setPlaceholderText(tr("Enter number here"));
  connect(_valueEdit, &ValueEdit::returnPressed, this, &MainWindow::push);

  _randomButton = new QPushButton(tr("Random"));
  _randomButton->setToolTip(
      tr("Generates random number and inserts it in the field"));
  connect(_randomButton, &QPushButton::clicked, _valueEdit,
          &ValueEdit::setRandomValue);

  _pushButton = new QPushButton(tr("Push"));
  _pushButton->setToolTip(tr("Pushes a number in the field to the tree"));
  connect(_pushButton, &QPushButton::clicked, this, &MainWindow::push);

  _popButton = new QPushButton(tr("Pop"));
  _popButton->setToolTip(tr("Pops a number in the field from the tree"));
  connect(_popButton, &QPushButton::clicked, this, &MainWindow::pop);

  _previousButton = new QPushButton(tr("Previous"));
  _previousButton->setToolTip(
      tr("Finds first number less than number in the field in the tree"));
  connect(_previousButton, &QPushButton::clicked, this, &MainWindow::previous);

  _nextButton = new QPushButton(tr("Next"));
  _nextButton->setToolTip(
      tr("Finds first number greater than number in the field in the tree"));
  connect(_nextButton, &QPushButton::clicked, this, &MainWindow::next);

  _getButton = new QPushButton(tr("Find"));
  _getButton->setToolTip(tr("Finds a number in the field in the tree"));
  connect(_getButton, &QPushButton::clicked, this, &MainWindow::get);

  QGridLayout *valueActionsBoxLayout = new QGridLayout();
  valueActionsBoxLayout->addWidget(_valueEdit, 0, 0, 1, 3);
  valueActionsBoxLayout->addWidget(_randomButton, 0, 3, 1, 1);
  valueActionsBoxLayout->addWidget(_pushButton, 1, 0, 1, 2);
  valueActionsBoxLayout->addWidget(_popButton, 1, 2, 1, 2);
  valueActionsBoxLayout->addWidget(_previousButton, 2, 0, 1, 2);
  valueActionsBoxLayout->addWidget(_nextButton, 2, 2, 1, 2);
  valueActionsBoxLayout->addWidget(_getButton, 3, 0, 1, 4);

  valueActionsBox->setLayout(valueActionsBoxLayout);

  QGroupBox *treeActionsBox = new QGroupBox(tr("Tree Actions"));

  _minimumButton = new QPushButton(tr("Minimum"));
  _minimumButton->setToolTip(tr("Finds the least number in the tree"));
  connect(_minimumButton, &QPushButton::clicked, this, &MainWindow::minimum);

  _maximumButton = new QPushButton(tr("Maximum"));
  _maximumButton->setToolTip(tr("Finds the greatest number in the tree"));
  connect(_maximumButton, &QPushButton::clicked, this, &MainWindow::maximum);

  _clearButton = new QPushButton(tr("Clear"));
  _clearButton->setToolTip(tr("Pops all numbers from the tree"));
  connect(_clearButton, &QPushButton::clicked, this, &MainWindow::clear);

  _zoomInButton = new QPushButton(tr("Zoom In"));
  _zoomInButton->setIcon(QIcon::fromTheme(QStringLiteral("zoom-in")));
  _zoomInButton->setShortcut(QKeySequence::ZoomIn);
  _zoomInButton->setToolTip(tr("Zooms In"));
  connect(_zoomInButton, &QPushButton::clicked, this, &MainWindow::zoomIn);

  _zoomOutButton = new QPushButton(tr("Zoom Out"));
  _zoomOutButton->setIcon(QIcon::fromTheme(QStringLiteral("zoom-out")));
  _zoomOutButton->setShortcut(QKeySequence::ZoomOut);
  _zoomOutButton->setToolTip(tr("Zooms Out"));
  connect(_zoomOutButton, &QPushButton::clicked, this, &MainWindow::zoomOut);

  _sizeLabel = new QLabel();
  connect(this, &MainWindow::sizeChanged, this, &MainWindow::updateSize);

  QGridLayout *treeActionsBoxLayout = new QGridLayout();
  treeActionsBoxLayout->addWidget(_minimumButton, 0, 0, 1, 2);
  treeActionsBoxLayout->addWidget(_maximumButton, 0, 2, 1, 2);
  treeActionsBoxLayout->addWidget(_clearButton, 1, 0, 1, 4);
  treeActionsBoxLayout->addWidget(_zoomInButton, 2, 0, 1, 2);
  treeActionsBoxLayout->addWidget(_zoomOutButton, 2, 2, 1, 2);
  treeActionsBoxLayout->addWidget(_sizeLabel, 3, 0, 1, 4, Qt::AlignCenter);

  treeActionsBox->setLayout(treeActionsBoxLayout);

  QGroupBox *treeTypeBox = new QGroupBox(tr("Tree Type"));

  _searchTreeRadio = new QRadioButton(tr("Search Tree"));
  _searchTreeRadio->setChecked(true);
  connect(_searchTreeRadio, &QAbstractButton::toggled, this,
          &MainWindow::switchTree);

  _randomizedTreeRadio = new QRadioButton(tr("Randomized Tree"));
  connect(_randomizedTreeRadio, &QAbstractButton::toggled, this,
          &MainWindow::switchTree);

  _avlTreeRadio = new QRadioButton(tr("AVL Tree"));
  connect(_avlTreeRadio, &QRadioButton::toggled, this, &MainWindow::switchTree);

  _redBlackTreeRadio = new QRadioButton(tr("Red-Black Tree"));
  connect(_redBlackTreeRadio, &QRadioButton::toggled, this,
          &MainWindow::switchTree);

  _splayTreeRadio = new QRadioButton(tr("Splay Tree"));
  connect(_splayTreeRadio, &QRadioButton::toggled, this,
          &MainWindow::switchTree);

  _skewedTreeRadio = new QRadioButton(tr("Skewed Tree"));
  connect(_skewedTreeRadio, &QRadioButton::toggled, this,
          &MainWindow::switchTree);

  QVBoxLayout *treeTypeBoxLayout = new QVBoxLayout();
  treeTypeBoxLayout->addWidget(_searchTreeRadio);
  treeTypeBoxLayout->addWidget(_randomizedTreeRadio);
  treeTypeBoxLayout->addWidget(_avlTreeRadio);
  treeTypeBoxLayout->addWidget(_redBlackTreeRadio);
  treeTypeBoxLayout->addWidget(_splayTreeRadio);
  treeTypeBoxLayout->addWidget(_skewedTreeRadio);

  treeTypeBox->setLayout(treeTypeBoxLayout);

  QVBoxLayout *actionsLayout = new QVBoxLayout();
  actionsLayout->addWidget(valueActionsBox);
  actionsLayout->addWidget(treeActionsBox);
  actionsLayout->addWidget(treeTypeBox);
  actionsLayout->addStretch();

  QHBoxLayout *mainLayout = new QHBoxLayout();
  mainLayout->addWidget(_treeWidget);
  mainLayout->addLayout(actionsLayout);
  mainWidget->setLayout(mainLayout);
  setCentralWidget(mainWidget);

  switchTree();
  updateSize();
}

quint32 MainWindow::treeSize() const {
  quint32 binarySearchTreeSize = _searchTree->size();
  assert(binarySearchTreeSize == _randomizedTree->size());
  assert(binarySearchTreeSize == _avlTree->size());
  assert(binarySearchTreeSize == _redBlackTree->size());
  assert(binarySearchTreeSize == _splayTree->size());
  assert(binarySearchTreeSize == _skewedTree->size());
  return binarySearchTreeSize;
}

void MainWindow::push() {
  if (_valueEdit->hasValue()) {
    qint32 value = _valueEdit->value();
    _searchTree->push(value);
    _randomizedTree->push(value);
    _avlTree->push(value);
    _redBlackTree->push(value);
    _splayTree->push(value);
    _skewedTree->push(value);
    emit sizeChanged();
  }
}

void MainWindow::pop() {
  if (_valueEdit->hasValue()) {
    qint32 value = _valueEdit->value();
    _searchTree->pop(value);
    _randomizedTree->pop(value);
    _avlTree->pop(value);
    _redBlackTree->pop(value);
    _splayTree->pop(value);
    _skewedTree->pop(value);
    emit sizeChanged();
  }
}

void MainWindow::previous() {
  if (_valueEdit->hasValue()) {
    qint32 value = _valueEdit->value();
    _searchTree->previous(value);
    _randomizedTree->previous(value);
    _avlTree->previous(value);
    _redBlackTree->previous(value);
    _splayTree->previous(value);
    _skewedTree->previous(value);
  }
}

void MainWindow::next() {
  if (_valueEdit->hasValue()) {
    qint32 value = _valueEdit->value();
    _searchTree->next(value);
    _randomizedTree->next(value);
    _avlTree->next(value);
    _redBlackTree->next(value);
    _splayTree->next(value);
    _skewedTree->next(value);
  }
}

void MainWindow::get() {
  if (_valueEdit->hasValue()) {
    qint32 value = _valueEdit->value();
    _searchTree->get(value);
    _randomizedTree->get(value);
    _avlTree->get(value);
    _redBlackTree->get(value);
    _splayTree->get(value);
    _skewedTree->get(value);
  }
}

void MainWindow::minimum() {
  _searchTree->minimum();
  _randomizedTree->minimum();
  _avlTree->minimum();
  _redBlackTree->minimum();
  _splayTree->minimum();
  _skewedTree->minimum();
}

void MainWindow::maximum() {
  _searchTree->maximum();
  _randomizedTree->maximum();
  _avlTree->maximum();
  _redBlackTree->maximum();
  _splayTree->maximum();
  _skewedTree->maximum();
}

void MainWindow::clear() {
  _searchTree->clear();
  _randomizedTree->clear();
  _avlTree->clear();
  _redBlackTree->clear();
  _splayTree->clear();
  _skewedTree->clear();
  emit sizeChanged();
}

void MainWindow::switchTree() {
  QString baseTitle = tr("Tree Visualizer");
  if (_searchTreeRadio->isChecked()) {
    _treeWidget->setCurrentWidget(_searchTree);
    setWindowTitle(baseTitle + " - " + _searchTreeRadio->text());
  } else if (_randomizedTreeRadio->isChecked()) {
    _treeWidget->setCurrentWidget(_randomizedTree);
    setWindowTitle(baseTitle + " - " + _randomizedTreeRadio->text());
  } else if (_avlTreeRadio->isChecked()) {
    _treeWidget->setCurrentWidget(_avlTree);
    setWindowTitle(baseTitle + " - " + _avlTreeRadio->text());
  } else if (_redBlackTreeRadio->isChecked()) {
    _treeWidget->setCurrentWidget(_redBlackTree);
    setWindowTitle(baseTitle + " - " + _redBlackTreeRadio->text());
  } else if (_splayTreeRadio->isChecked()) {
    _treeWidget->setCurrentWidget(_splayTree);
    setWindowTitle(baseTitle + " - " + _splayTreeRadio->text());
  } else if (_skewedTreeRadio->isChecked()) {
    _treeWidget->setCurrentWidget(_skewedTree);
    setWindowTitle(baseTitle + " - " + _skewedTreeRadio->text());
  }
}

void MainWindow::updateSize() {
  _sizeLabel->setText(tr("Size of tree: %1").arg(treeSize()));
}

void MainWindow::zoomIn() {
  auto *tree = dynamic_cast<Tree<qint32> *>(_treeWidget->currentWidget());
  tree->zoomIn();
}

void MainWindow::zoomOut() {
  auto *tree = dynamic_cast<Tree<qint32> *>(_treeWidget->currentWidget());
  tree->zoomOut();
}

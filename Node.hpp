﻿#ifndef NODE_HPP
#define NODE_HPP

#include <QGraphicsDropShadowEffect>
#include <QGraphicsItem>

template <class T> class Tree;
template <class T> class Edges;

template <class T> class Node : public QGraphicsItem {
public:
  static Node *left(Node<T> *);
  static Node<T> *right(Node<T> *);

  static constexpr const qint32 Radius = 30;

  Node(Tree<T> *);
  Node(Tree<T> *, const T &);
  ~Node() override;

  virtual QColor color() const;

  virtual Node<T> *left() const;
  void setLeft(Node<T> *);

  virtual Node<T> *right() const;
  void setRight(Node<T> *);

  const T &value() const;
  void setValue(const T &);
  void swapValues(Node<T> *);

  QRectF boundingRect() const override;
  QPainterPath shape() const override;
  void paint(QPainter *, const QStyleOptionGraphicsItem *, QWidget *) override;

  Edges<T> *edges() const;

  qint32 modifier() const;
  void setModifier(qint32);

private:
  Node(Tree<T> *, T *);

  Tree<T> *_tree;

  Node<T> *_left;
  Node<T> *_right;

  Edges<T> *_edges;

  T *_value;
  qint32 _modifier;
};

#include "Edges.hpp"
#include <QPainter>
#include <QtMath>

template <class T> Node<T>::Node(Tree<T> *tree) : Node<T>(tree, nullptr) {}

template <class T>
Node<T>::Node(Tree<T> *tree, const T &value) : Node(tree, new T(value)) {}

template <class T> Node<T>::~Node() {
  --_tree->_size;
  delete _value;
  delete _edges;
}

template <class T> QColor Node<T>::color() const {
  return QColor(33, 150, 243);
}

template <class T> Node<T> *Node<T>::left(Node *node) {
  return node == nullptr ? nullptr : node->left();
}

template <class T> Node<T> *Node<T>::left() const { return _left; }

template <class T> void Node<T>::setLeft(Node *newLeft) { _left = newLeft; }

template <class T> Node<T> *Node<T>::right(Node *node) {
  return node == nullptr ? nullptr : node->right();
}

template <class T> Node<T> *Node<T>::right() const { return _right; }

template <class T> void Node<T>::setRight(Node *newRight) { _right = newRight; }

template <class T> const T &Node<T>::value() const { return *_value; }

template <class T> void Node<T>::setValue(const T &newValue) {
  delete _value;
  _value = new T(newValue);
  update();
}

template <class T> void Node<T>::swapValues(Node<T> *node) {
  if (node == nullptr)
    return;
  T *temp = _value;
  _value = node->_value;
  node->_value = temp;
  update();
  node->update();
}

template <class T> QRectF Node<T>::boundingRect() const {
  return QRectF(-2 * Radius, -2 * Radius, 4 * Radius, 4 * Radius);
}

template <class T> QPainterPath Node<T>::shape() const {
  QPainterPath path;
  path.addEllipse(-Radius, -Radius, 2 * Radius, 2 * Radius);
  return path;
}

template <class T>
void Node<T>::paint(QPainter *painter, const QStyleOptionGraphicsItem *,
                    QWidget *) {
  painter->setPen(Qt::NoPen);
  painter->setBrush(color());
  painter->drawEllipse(-Radius, -Radius, 2 * Radius, 2 * Radius);

  painter->setPen(QPen(Qt::white, 1));
  QString num = QString::number(value());

  QFont font = painter->font();
  font.setPixelSize(Radius);
  QRectF fontRect = QFontMetricsF(font).boundingRect(num);
  qreal widthSquared = fontRect.width() * fontRect.width();
  qreal heightSquared = fontRect.height() * fontRect.height();
  qreal diagonal = qSqrt(widthSquared + heightSquared);
  qreal fontScale = 2 * Radius / diagonal;
  font.setPixelSize(static_cast<qint32>(Radius * fontScale));
  font.setBold(true);
  painter->setFont(font);

  painter->drawText(boundingRect(), Qt::AlignCenter, num);
}

template <class T> Edges<T> *Node<T>::edges() const { return _edges; }

template <class T> qint32 Node<T>::modifier() const { return _modifier; }

template <class T> void Node<T>::setModifier(qint32 value) {
  _modifier = value;
}

template <class T>
Node<T>::Node(Tree<T> *tree, T *value)
    : _tree(tree), _left(nullptr), _right(nullptr), _edges(new Edges<T>(this)),
      _value(value), _modifier(0) {
  setCacheMode(DeviceCoordinateCache);

  ++tree->_size;
  tree->scene()->addItem(this);
  tree->scene()->addItem(_edges);
}

#endif // NODE_HPP

﻿#ifndef SPLAYNODE_HPP
#define SPLAYNODE_HPP

#include "Node.hpp"

template <class T> class SplayTree;

template <class T> class SplayNode : public Node<T> {
public:
  SplayNode(SplayTree<T> *);
  SplayNode(SplayTree<T> *, const T &);

  SplayNode<T> *left() const override;
  SplayNode<T> *right() const override;

  QColor color() const override;
};

#include "SplayTree.hpp"

template <class T>
SplayNode<T>::SplayNode(SplayTree<T> *tree) : Node<T>(tree) {}

template <class T>
SplayNode<T>::SplayNode(SplayTree<T> *tree, const T &value)
    : Node<T>(tree, value) {}

template <class T> SplayNode<T> *SplayNode<T>::left() const {
  return static_cast<SplayNode<T> *>(Node<T>::left());
}

template <class T> SplayNode<T> *SplayNode<T>::right() const {
  return static_cast<SplayNode<T> *>(Node<T>::right());
}

template <class T> QColor SplayNode<T>::color() const {
  return QColor(156, 39, 176);
}

#endif // SPLAYNODE_HPP

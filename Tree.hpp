﻿#ifndef TREE_HPP
#define TREE_HPP

#include "DraggableView.hpp"
#include "ZoomableView.hpp"

template <class T> class Node;
template <class T> class Highlighter;

template <class T> class Tree : public DraggableView, public ZoomableView {
public:
  Tree(QWidget * = nullptr);
  ~Tree() override;

  quint32 size() const;
  void clear();

public slots:
  void zoomIn();
  void zoomOut();

protected:
  Tree(Node<T> *, QWidget * = nullptr);

  Node<T> *clear(Node<T> *);
  Node<T> *root() const;
  void setRoot(Node<T> *value);

  void highlight(Node<T> * = nullptr);

  void adjust();
  void adjustFirstWalk(Node<T> *, qint32 = 1);
  void adjustSecondWalk(Node<T> *, qint32 = 1, qint32 = 0);

private:
  static constexpr const qreal ZoomFactor = 1.25;
  static constexpr const qint32 LevelHeight = 3.5 * Node<T>::Radius;
  static constexpr const qint32 LevelWidth = 2.5 * Node<T>::Radius;

  Node<T> *_root;
  Highlighter<T> *_highlighter;

  qint32 *_modifier;
  qint32 *_nextPos;
  quint32 _size;

  friend class Node<T>;
};

#include "Highlighter.hpp"
#include "Node.hpp"
#include <QKeyEvent>
#include <QMouseEvent>
#include <QScrollBar>
#include <QWheelEvent>

template <class T>
Tree<T>::Tree(QWidget *parent)
    : DraggableView(parent), ZoomableView(parent), _root(nullptr),
      _highlighter(new Highlighter<T>()), _modifier(nullptr), _nextPos(nullptr),
      _size(0) {
  setScene(new QGraphicsScene(this));
  scene()->setItemIndexMethod(QGraphicsScene::NoIndex);
  setCacheMode(CacheBackground);
  setViewportUpdateMode(FullViewportUpdate);
  setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
  setTransformationAnchor(AnchorUnderMouse);
  setMinimumSize(400, 400);

  setBackgroundBrush(QBrush(QColor(38, 50, 56)));

  scene()->addItem(_highlighter);
}

template <class T> Tree<T>::~Tree() { clear(_root); }

template <class T> quint32 Tree<T>::size() const { return _size; }

template <class T> void Tree<T>::clear() {
  highlight();
  setRoot(clear(root()));
}

template <class T> void Tree<T>::zoomIn() { scaleView(ZoomFactor); }

template <class T> void Tree<T>::zoomOut() { scaleView(1 / ZoomFactor); }

template <class T>
Tree<T>::Tree(Node<T> *root, QWidget *parent)
    : QGraphicsView(parent), _root(root) {}

template <class T> Node<T> *Tree<T>::clear(Node<T> *node) {
  if (node != nullptr) {
    clear(node->left());
    clear(node->right());
    delete node;
  }
  return nullptr;
}

template <class T> Node<T> *Tree<T>::root() const { return _root; }

template <class T> void Tree<T>::setRoot(Node<T> *newRoot) { _root = newRoot; }

template <class T> void Tree<T>::highlight(Node<T> *node) {
  _highlighter->setNode(node);
  _highlighter->update();
}

template <class T> void Tree<T>::adjust() {
  quint32 maxHeight = size();
  _modifier = new qint32[maxHeight + 1];
  _nextPos = new qint32[maxHeight + 1];

  for (quint32 i = 0; i <= maxHeight; i++)
    _modifier[i] = _nextPos[i] = 0;

  adjustFirstWalk(root());
  adjustSecondWalk(root());

  delete[] _modifier;
  delete[] _nextPos;

  _modifier = nullptr;
  _nextPos = nullptr;
}

template <class T> void Tree<T>::adjustFirstWalk(Node<T> *node, qint32 depth) {
  if (node != nullptr) {
    adjustFirstWalk(node->left(), depth + 1);
    adjustFirstWalk(node->right(), depth + 1);

    qint32 place;

    if (node->left() == nullptr && node->right() == nullptr) {
      place = _nextPos[depth];
    } else if (node->left() == nullptr) {
      place = static_cast<qint32>(node->right()->x()) - LevelWidth / 4;
    } else if (node->right() == nullptr) {
      place = static_cast<qint32>(node->left()->x()) + LevelWidth / 4;
    } else {
      place = static_cast<qint32>(node->left()->x() + node->right()->x()) / 2;
    }

    _modifier[depth] = qMax(_modifier[depth], _nextPos[depth] - place);
    node->setModifier(_modifier[depth]);

    if (node->left() == nullptr && node->right() == nullptr) {
      node->setX(place);
    } else {
      node->setX(place + _modifier[depth]);
    }

    _nextPos[depth] = static_cast<qint32>(node->x()) + LevelWidth;
  }
}

template <class T>
void Tree<T>::adjustSecondWalk(Node<T> *node, qint32 depth,
                               qint32 modifierSum) {
  if (node != nullptr) {
    adjustSecondWalk(node->left(), depth + 1, modifierSum + node->modifier());
    adjustSecondWalk(node->right(), depth + 1, modifierSum + node->modifier());

    node->setX(node->x() + modifierSum);
    node->setY(LevelHeight * depth);

    node->update();
    node->edges()->update();
  }
}

#endif // TREE_HPP

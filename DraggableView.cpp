﻿#include "DraggableView.hpp"
#include <QMouseEvent>
#include <QScrollBar>

DraggableView::DraggableView(QWidget *parent)
    : QGraphicsView(parent), _origin(QPointF()) {}

void DraggableView::mousePressEvent(QMouseEvent *event) {
  if (event->button() == Qt::LeftButton) {
    _origin = event->pos();
    setCursor(Qt::ClosedHandCursor);
    event->accept();
  }
}

void DraggableView::mouseReleaseEvent(QMouseEvent *event) {
  if (event->button() == Qt::LeftButton) {
    setCursor(Qt::ArrowCursor);
    event->accept();
  }
}

void DraggableView::mouseMoveEvent(QMouseEvent *event) {
  if (event->buttons() & Qt::LeftButton) {
    QPoint delta = (event->pos() - _origin).toPoint();
    qint32 x = horizontalScrollBar()->value() - delta.x();
    qint32 y = verticalScrollBar()->value() - delta.y();
    horizontalScrollBar()->setValue(x);
    verticalScrollBar()->setValue(y);
    _origin = event->pos();
    event->accept();
  }
}

﻿#ifndef AVLNODE_HPP
#define AVLNODE_HPP

#include "Node.hpp"

template <class T> class AVLTree;

template <class T> class AVLNode : public Node<T> {
public:
  static quint8 height(AVLNode<T> *);

  AVLNode(AVLTree<T> *);
  AVLNode(AVLTree<T> *, const T &);

  AVLNode<T> *left() const override;
  AVLNode<T> *right() const override;

  QColor color() const override;

  quint8 height() const;
  void setHeight();

private:
  quint8 _height;
};

#include "AVLTree.hpp"

template <class T> quint8 AVLNode<T>::height(AVLNode *node) {
  return node == nullptr ? 0 : node->height();
}

template <class T>
AVLNode<T>::AVLNode(AVLTree<T> *tree) : Node<T>(tree), _height(1) {}

template <class T>
AVLNode<T>::AVLNode(AVLTree<T> *tree, const T &value)
    : Node<T>(tree, value), _height(1) {}

template <class T> AVLNode<T> *AVLNode<T>::left() const {
  return static_cast<AVLNode<T> *>(Node<T>::left());
}

template <class T> AVLNode<T> *AVLNode<T>::right() const {
  return static_cast<AVLNode<T> *>(Node<T>::right());
}

template <class T> QColor AVLNode<T>::color() const {
  return QColor(76, 175, 80);
}

template <class T> quint8 AVLNode<T>::height() const { return _height; }

template <class T> void AVLNode<T>::setHeight() {
  quint8 leftHeight = height(left());
  quint8 rightHeight = height(right());
  _height = qMax(leftHeight, rightHeight) + 1;
}

#endif // AVLNODE_HPP

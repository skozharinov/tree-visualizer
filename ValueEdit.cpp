﻿#include "ValueEdit.hpp"
#include <QIntValidator>
#include <QKeyEvent>
#include <QRandomGenerator>

ValueEdit::ValueEdit(QWidget *parent) : QLineEdit(parent) {
  setValidator(new QIntValidator(this));
}

bool ValueEdit::hasValue() const { return !text().isEmpty(); }

qint32 ValueEdit::value() {
  qint32 value = text().toInt();
  clear();
  setFocus();
  return value;
}

void ValueEdit::setRandomValue() {
  quint32 power = QRandomGenerator::global()->bounded(0U, 32U) + 1;
  qint32 value = static_cast<qint32>(QRandomGenerator::global()->generate());

  if (value >= 0) {
    value &= ((1 << power) - 1);
  } else {
    value = ~((~value - 1) & ((1 << power) - 1)) - 1;
  }

  setText(QString::number(value));
  setFocus();
}

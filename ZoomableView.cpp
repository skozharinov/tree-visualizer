﻿#include "ZoomableView.hpp"
#include <QWheelEvent>
#include <QtMath>

ZoomableView::ZoomableView(QWidget *parent) : QGraphicsView(parent) {}

void ZoomableView::wheelEvent(QWheelEvent *event) {
  scaleView(qPow(2, event->angleDelta().y() / 480.0));
  event->accept();
}

void ZoomableView::scaleView(qreal scaleFactor) {
  qreal factor = newScaleFactor(scaleFactor);
  if (factor >= ScaleMin && factor <= ScaleMax)
    scale(scaleFactor, scaleFactor);
}

qreal ZoomableView::newScaleFactor(qreal factor) {
  constexpr const QRectF unit(0, 0, 1, 1);
  return transform().scale(factor, factor).mapRect(unit).width();
}

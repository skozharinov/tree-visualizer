﻿#ifndef SKEWEDNODE_HPP
#define SKEWEDNODE_HPP

#include "Node.hpp"

template <class T> class SkewedTree;

template <class T> class SkewedNode : public Node<T> {
public:
  SkewedNode(SkewedTree<T> *);
  SkewedNode(SkewedTree<T> *, const T &);

  SkewedNode<T> *left() const override;
  SkewedNode<T> *right() const override;

  QColor color() const override;
};

#include "SkewedTree.hpp"

template <class T>
SkewedNode<T>::SkewedNode(SkewedTree<T> *tree) : Node<T>(tree) {}

template <class T>
SkewedNode<T>::SkewedNode(SkewedTree<T> *tree, const T &value)
    : Node<T>(tree, value) {}

template <class T> SkewedNode<T> *SkewedNode<T>::left() const {
  return static_cast<SkewedNode<T> *>(Node<T>::left());
}

template <class T> SkewedNode<T> *SkewedNode<T>::right() const {
  return nullptr;
}

template <class T> QColor SkewedNode<T>::color() const {
  return QColor(0, 150, 136);
}

#endif // SKEWEDNODE_HPP

﻿#include "MainWindow.hpp"
#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);

  QTranslator translator;

  if (translator.load(QLocale(), QStringLiteral("translation"),
                      QStringLiteral("_"), QStringLiteral("://"))) {
    app.installTranslator(&translator);
  }

  MainWindow window;
  window.show();

  return app.exec();
}

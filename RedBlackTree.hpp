﻿#ifndef REDBLACKTREE_HPP
#define REDBLACKTREE_HPP

#include "SearchTree.hpp"

template <class T> class RedBlackNode;

template <class T> class RedBlackTree : public SearchTree<T> {
public:
  void push(const T &);
  void pop(const T &);

protected:
  RedBlackNode<T> *doPush(RedBlackNode<T> *, const T &);
  RedBlackNode<T> *doPop(RedBlackNode<T> *, const T &);

  void rotateLeft(RedBlackNode<T> *);
  void rotateRight(RedBlackNode<T> *);

  void fixPush(RedBlackNode<T> *);
  void fixPop(RedBlackNode<T> *);

private:
  RedBlackNode<T> *cast(Node<T> *);
};

#include "RedBlackNode.hpp"

template <class T> void RedBlackTree<T>::push(const T &value) {
  Tree<T>::highlight();
  RedBlackNode<T> *node = doPush(cast(Tree<T>::root()), value);

  if (node != nullptr) {
    fixPush(node);
    Tree<T>::adjust();
    Tree<T>::highlight(node);
  }
}

template <class T> void RedBlackTree<T>::pop(const T &value) {
  Tree<T>::highlight();
  RedBlackNode<T> *node = doPop(cast(Tree<T>::root()), value);

  if (node != nullptr) {
    fixPop(node);
    Tree<T>::adjust();
  }
}

template <class T>
RedBlackNode<T> *RedBlackTree<T>::doPush(RedBlackNode<T> *root,
                                         const T &value) {
  RedBlackNode<T> *node = nullptr;
  if (root == nullptr) {
    root = new RedBlackNode<T>(this, value);
    Tree<T>::setRoot(root);
    node = root;
  } else if (value < root->value()) {
    if (root->left() == nullptr) {
      root->setLeft(new RedBlackNode<T>(this, value));
      root->left()->setParent(root);
      node = root->left();
    } else {
      node = doPush(root->left(), value);
    }
  } else if (value > root->value()) {
    if (root->right() == nullptr) {
      root->setRight(new RedBlackNode<T>(this, value));
      root->right()->setParent(root);
      node = root->right();
    } else {
      node = doPush(root->right(), value);
    }
  }

  return node;
}

template <class T>
RedBlackNode<T> *RedBlackTree<T>::doPop(RedBlackNode<T> *root, const T &value) {
  if (root == nullptr) {
    return nullptr;
  } else if (value < root->value()) {
    return doPop(root->left(), value);
  } else if (value > root->value()) {
    return doPop(root->right(), value);
  } else {
    if (root->left() == nullptr || root->right() == nullptr) {
      return root;
    } else {
      RedBlackNode<T> *temp = cast(SearchTree<T>::minimum(root->right()));
      root->swapValues(temp);
      return doPop(root->right(), value);
    }
  }
}

template <class T> void RedBlackTree<T>::rotateLeft(RedBlackNode<T> *node) {
  RedBlackNode<T> *temp = node->right();
  node->setRight(temp->left());

  if (node->right() != nullptr)
    node->right()->setParent(node);

  temp->setParent(node->parent());

  if (node->parent() == nullptr)
    Tree<T>::setRoot(temp);
  else if (node->parent()->left() == node)
    node->parent()->setLeft(temp);
  else
    node->parent()->setRight(temp);

  temp->setLeft(node);
  node->setParent(temp);
}

template <class T> void RedBlackTree<T>::rotateRight(RedBlackNode<T> *node) {
  RedBlackNode<T> *temp = node->left();
  node->setLeft(temp->right());

  if (node->left() != nullptr)
    node->left()->setParent(node);

  temp->setParent(node->parent());

  if (node->parent() == nullptr)
    Tree<T>::setRoot(temp);
  else if (node->parent()->left() == node)
    node->parent()->setLeft(temp);
  else
    node->parent()->setRight(temp);

  temp->setRight(node);
  node->setParent(temp);
}

template <class T> void RedBlackTree<T>::fixPush(RedBlackNode<T> *node) {
  while (node->parent() != nullptr &&
         node->nodeColor() == RedBlackNode<T>::Red &&
         node->parent()->nodeColor() == RedBlackNode<T>::Red) {
    RedBlackNode<T> *parent = node->parent();
    RedBlackNode<T> *grandParent = parent->parent();
    if (parent == grandParent->left()) {
      RedBlackNode<T> *uncle = grandParent->right();
      if (uncle != nullptr && uncle->nodeColor() == RedBlackNode<T>::Red) {
        parent->setNodeColor(RedBlackNode<T>::Black);
        uncle->setNodeColor(RedBlackNode<T>::Black);
        grandParent->setNodeColor(RedBlackNode<T>::Red);
        node = grandParent;
      } else {
        if (node == parent->right()) {
          rotateLeft(parent);
          node = parent;
          parent = node->parent();
        }
        rotateRight(grandParent);
        parent->swapNodeColors(grandParent);
        node = parent;
      }
    } else {
      RedBlackNode<T> *uncle = grandParent->left();
      if (uncle != nullptr && uncle->nodeColor() == RedBlackNode<T>::Red) {
        parent->setNodeColor(RedBlackNode<T>::Black);
        uncle->setNodeColor(RedBlackNode<T>::Black);
        grandParent->setNodeColor(RedBlackNode<T>::Red);
        node = grandParent;
      } else {
        if (node == parent->left()) {
          rotateRight(parent);
          node = parent;
          parent = node->parent();
        }
        rotateLeft(grandParent);
        parent->swapNodeColors(grandParent);
        node = parent;
      }
    }
  }

  cast(Tree<T>::root())->setNodeColor(RedBlackNode<T>::Black);
}

template <class T> void RedBlackTree<T>::fixPop(RedBlackNode<T> *node) {
  if (node == Tree<T>::root()) {
    if (node->left() != nullptr) {
      node->left()->setNodeColor(RedBlackNode<T>::Black);
      Tree<T>::setRoot(node->left());
    } else if (node->right() != nullptr) {
      node->right()->setNodeColor(RedBlackNode<T>::Black);
      Tree<T>::setRoot(node->right());
    } else {
      Tree<T>::setRoot(nullptr);
    }

    delete node;
  } else if (RedBlackNode<T>::nodeColor(node) == RedBlackNode<T>::Red ||
             RedBlackNode<T>::nodeColor(node->left()) == RedBlackNode<T>::Red ||
             RedBlackNode<T>::nodeColor(node->right()) ==
                 RedBlackNode<T>::Red) {
    RedBlackNode<T> *child =
        node->left() == nullptr ? node->right() : node->left();

    if (node == node->parent()->left())
      node->parent()->setLeft(child);
    else
      node->parent()->setRight(child);

    if (child != nullptr) {
      child->setParent(node->parent());
      child->setNodeColor(RedBlackNode<T>::Black);
    }

    delete node;
  } else {
    RedBlackNode<T> *ptr = node;
    ptr->setNodeColor(RedBlackNode<T>::DoubleBlack);
    while (ptr->parent() != nullptr &&
           RedBlackNode<T>::nodeColor(ptr) == RedBlackNode<T>::DoubleBlack) {
      RedBlackNode<T> *parent = ptr->parent();
      if (ptr == parent->left()) {
        RedBlackNode<T> *sibling = parent->right();

        if (RedBlackNode<T>::nodeColor(sibling) == RedBlackNode<T>::Red) {
          sibling->setNodeColor(RedBlackNode<T>::Black);
          parent->setNodeColor(RedBlackNode<T>::Red);
          rotateLeft(parent);
        } else {
          if (RedBlackNode<T>::nodeColor(sibling->left()) ==
                  RedBlackNode<T>::Black &&
              RedBlackNode<T>::nodeColor(sibling->right()) ==
                  RedBlackNode<T>::Black) {
            sibling->setNodeColor(RedBlackNode<T>::Red);

            if (RedBlackNode<T>::nodeColor(parent) == RedBlackNode<T>::Red)
              parent->setNodeColor(RedBlackNode<T>::Black);
            else
              parent->setNodeColor(RedBlackNode<T>::DoubleBlack);

            ptr->setNodeColor(RedBlackNode<T>::Black);
            ptr = parent;
          } else {
            if (RedBlackNode<T>::nodeColor(sibling->right()) ==
                RedBlackNode<T>::Black) {
              sibling->left()->setNodeColor(RedBlackNode<T>::Black);
              sibling->setNodeColor(RedBlackNode<T>::Red);
              rotateRight(sibling);
              sibling = parent->right();
            }

            ptr->setNodeColor(RedBlackNode<T>::Black);
            sibling->setNodeColor(RedBlackNode<T>::nodeColor(parent));
            parent->setNodeColor(RedBlackNode<T>::Black);
            sibling->right()->setNodeColor(RedBlackNode<T>::Black);
            rotateLeft(parent);
            break;
          }
        }
      } else {
        RedBlackNode<T> *sibling = parent->left();
        if (RedBlackNode<T>::nodeColor(sibling) == RedBlackNode<T>::Red) {
          sibling->setNodeColor(RedBlackNode<T>::Black);
          parent->setNodeColor(RedBlackNode<T>::Red);
          rotateRight(parent);
        } else {
          if (RedBlackNode<T>::nodeColor(sibling->left()) ==
                  RedBlackNode<T>::Black &&
              RedBlackNode<T>::nodeColor(sibling->right()) ==
                  RedBlackNode<T>::Black) {
            sibling->setNodeColor(RedBlackNode<T>::Red);
            if (RedBlackNode<T>::nodeColor(parent) == RedBlackNode<T>::Red)
              parent->setNodeColor(RedBlackNode<T>::Black);
            else
              parent->setNodeColor(RedBlackNode<T>::DoubleBlack);

            ptr->setNodeColor(RedBlackNode<T>::Black);
            ptr = parent;
          } else {
            if (RedBlackNode<T>::nodeColor(sibling->left()) ==
                RedBlackNode<T>::Black) {
              sibling->right()->setNodeColor(RedBlackNode<T>::Black);
              sibling->setNodeColor(RedBlackNode<T>::Red);
              rotateLeft(sibling);
              sibling = parent->left();
            }

            ptr->setNodeColor(RedBlackNode<T>::Black);
            sibling->setNodeColor(RedBlackNode<T>::nodeColor(parent));
            parent->setNodeColor(RedBlackNode<T>::Black);
            sibling->left()->setNodeColor(RedBlackNode<T>::Black);
            rotateRight(parent);
            break;
          }
        }
      }
    }

    RedBlackNode<T> *temp = node->parent();

    if (node == temp->left())
      temp->setLeft(nullptr);
    else
      temp->setRight(nullptr);

    delete node;

    cast(Tree<T>::root())->setNodeColor(RedBlackNode<T>::Black);
  }
}

template <class T> RedBlackNode<T> *RedBlackTree<T>::cast(Node<T> *node) {
  return static_cast<RedBlackNode<T> *>(node);
}

#endif // REDBLACKTREE_HPP

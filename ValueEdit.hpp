﻿#ifndef VALUEEDIT_HPP
#define VALUEEDIT_HPP

#include <QLineEdit>

class ValueEdit : public QLineEdit {
  Q_OBJECT
public:
  ValueEdit(QWidget * = nullptr);

  bool hasValue() const;
  qint32 value();

public slots:
  void setRandomValue();
};

#endif // VALUEEDIT_HPP

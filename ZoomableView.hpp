﻿#ifndef ZOOMABLEVIEW_HPP
#define ZOOMABLEVIEW_HPP

#include <QGraphicsView>

class ZoomableView : virtual public QGraphicsView {
public:
  ZoomableView(QWidget * = nullptr);

protected:
  static constexpr const qreal ScaleMin = 0.1;
  static constexpr const qreal ScaleMax = 10;

  void wheelEvent(QWheelEvent *) override;
  void scaleView(qreal);
  qreal newScaleFactor(qreal);
};

#endif // ZOOMABLEVIEW_HPP

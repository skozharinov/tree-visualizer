<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="MainWindow.cpp" line="51"/>
        <source>Push</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="52"/>
        <source>Pushes a number in the field to the tree</source>
        <translation>Добавляет в дерево число из поля</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="55"/>
        <source>Pop</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="56"/>
        <source>Pops a number in the field from the tree</source>
        <translation>Удаляет из дерева число из поля</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="59"/>
        <source>Previous</source>
        <translation>Предыдущий</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="61"/>
        <source>Finds first number less than number in the field in the tree</source>
        <translation>Находит в дереве первое число меньшее числа из поля</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="64"/>
        <source>Next</source>
        <translation>Следующий</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="66"/>
        <source>Finds first number greater than number in the field in the tree</source>
        <translation>Находит в дереве первое число большее числа из поля</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="69"/>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="70"/>
        <source>Finds a number in the field in the tree</source>
        <translation>Находит в дереве число из поля</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="84"/>
        <source>Tree Actions</source>
        <translation>Действия с деревом</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="86"/>
        <source>Minimum</source>
        <translation>Минимум</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="87"/>
        <source>Finds the least number in the tree</source>
        <translation>Находит в дереве наименьшее число</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="90"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="91"/>
        <source>Finds the greatest number in the tree</source>
        <translation>Находит в дереве наибольшее число</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="94"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="95"/>
        <source>Pops all numbers from the tree</source>
        <translation>Удаляет из дерева все числа</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="98"/>
        <source>Zoom In</source>
        <translation>Приблизить</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="101"/>
        <source>Zooms In</source>
        <translation>Приближает дерево</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="104"/>
        <source>Zoom Out</source>
        <translation>Отдалить</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="107"/>
        <source>Zooms Out</source>
        <translation>Отдаляет дерево</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="123"/>
        <source>Tree Type</source>
        <translation>Тип дерева</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="125"/>
        <source>Search Tree</source>
        <oldsource>Binary Search Tree</oldsource>
        <translation>Дерево поиска</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="145"/>
        <source>Skewed Tree</source>
        <translation>Бамбук</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="276"/>
        <source>Tree Visualizer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="299"/>
        <source>Size of tree: %1</source>
        <translation>Размер дерева: %1</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="134"/>
        <source>AVL Tree</source>
        <translation>АВЛ дерево</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="39"/>
        <source>Number Actions</source>
        <oldsource>Value Actions</oldsource>
        <translation>Действия с числами</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="42"/>
        <source>Enter number here</source>
        <oldsource>Enter value here</oldsource>
        <translation>Введите число</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="45"/>
        <source>Random</source>
        <translation>Случайное</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="47"/>
        <source>Generates random number and inserts it in the field</source>
        <translation>Генерирует случайное число и вставляет его в поле</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="130"/>
        <source>Randomized Tree</source>
        <translation>Рандомизированное дерево</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="137"/>
        <source>Red-Black Tree</source>
        <translation>Красно-чёрное дерево</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="141"/>
        <source>Splay Tree</source>
        <translation>Splay-дерево</translation>
    </message>
</context>
</TS>

﻿#ifndef DRAGGABLEVIEW_HPP
#define DRAGGABLEVIEW_HPP

#include <QGraphicsView>

class DraggableView : virtual public QGraphicsView {
public:
  DraggableView(QWidget * = nullptr);

protected:
  void mousePressEvent(QMouseEvent *) override;
  void mouseReleaseEvent(QMouseEvent *) override;
  void mouseMoveEvent(QMouseEvent *) override;

private:
  QPointF _origin;
};

#endif // DRAGGABLEVIEW_HPP
